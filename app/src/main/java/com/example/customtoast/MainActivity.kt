package com.example.customtoast

import android.os.Bundle
import android.widget.Adapter
import android.widget.SeekBar
import androidx.appcompat.app.AppCompatActivity
import com.example.customtoast.adapters.MyRecyclerAdapter
import com.example.customtoast.models.Person
import ja.burhanrashid52.photoeditor.PhotoEditor
import ja.burhanrashid52.photoeditor.PhotoEditorView
import ja.burhanrashid52.photoeditor.PhotoFilter
import kotlinx.android.synthetic.main.activity_main.*
import uz.mobiler.a4kfullwallpapers.models.filter_model.Filters


class MainActivity : AppCompatActivity() {

    private lateinit var listFilter: ArrayList<Filters>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initFilterList()

        val mPhotoEditor = PhotoEditor.Builder(this, photoView as PhotoEditorView)
            .setPinchTextScalable(true)
            .build()

        rv_list.adapter = MyRecyclerAdapter(listFilter, object : MyRecyclerAdapter.OnClick {
            override fun onClick(postion: Int) {
                mPhotoEditor.setFilterEffect(listFilter[postion].photoFilter)
                mPhotoEditor.setOpacity(10)
            }

        })

        seek.incrementProgressBy(1); // прогресс = 90
        seek.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                mPhotoEditor.setOpacity(progress)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {

            }

        })

    }

    private fun initFilterList() {

        listFilter = ArrayList()
        listFilter.add(Filters("None", PhotoFilter.NONE))
        listFilter.add(Filters("AutoFix", PhotoFilter.AUTO_FIX))
        listFilter.add(Filters("BlackWhite", PhotoFilter.BLACK_WHITE))
        listFilter.add(Filters("Brightness", PhotoFilter.BRIGHTNESS))
        listFilter.add(Filters("Contrast", PhotoFilter.CONTRAST))
        listFilter.add(Filters("CrossProcess", PhotoFilter.CROSS_PROCESS))
        listFilter.add(Filters("Documentary", PhotoFilter.DOCUMENTARY))
        listFilter.add(Filters("DuoTone", PhotoFilter.DUE_TONE))
        listFilter.add(Filters("Fill-Light", PhotoFilter.FILL_LIGHT))
        listFilter.add(Filters("FishEye", PhotoFilter.FISH_EYE))
        listFilter.add(Filters("Flip Horizontally", PhotoFilter.FLIP_HORIZONTAL))
        listFilter.add(Filters("Flip Vertically", PhotoFilter.FLIP_VERTICAL))
        listFilter.add(Filters("Grain", PhotoFilter.GRAIN))
        listFilter.add(Filters("Grayscale", PhotoFilter.GRAY_SCALE))
        listFilter.add(Filters("Lomish", PhotoFilter.LOMISH))
        listFilter.add(Filters("Negative", PhotoFilter.NEGATIVE))
        listFilter.add(Filters("Posterize", PhotoFilter.POSTERIZE))
        listFilter.add(Filters("Rotate", PhotoFilter.ROTATE))
        listFilter.add(Filters("Saturate", PhotoFilter.SATURATE))
        listFilter.add(Filters("Sepia", PhotoFilter.SEPIA))
        listFilter.add(Filters("Sepia", PhotoFilter.SHARPEN))
        listFilter.add(Filters("Temperature", PhotoFilter.TEMPERATURE))
        listFilter.add(Filters("Tint", PhotoFilter.TINT))
        listFilter.add(Filters("Vignette", PhotoFilter.VIGNETTE))
    }
}
