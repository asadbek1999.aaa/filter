package com.example.customtoast.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.customtoast.R
import kotlinx.android.synthetic.main.list_item.view.*
import uz.mobiler.a4kfullwallpapers.models.filter_model.Filters

class MyRecyclerAdapter(var list: List<Filters>, var listener: OnClick) :
    RecyclerView.Adapter<MyRecyclerAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun onBind(person: Filters) {
            itemView.filter_title.text = person.title

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)
        )
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.onBind(list[position])
        holder.itemView.setOnClickListener {
            listener.onClick(position)
        }
    }

    interface OnClick {
        fun onClick(postion: Int)
    }
}