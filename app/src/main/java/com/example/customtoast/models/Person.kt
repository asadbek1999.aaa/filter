package com.example.customtoast.models

data class Person(var name: String, var surname: String)