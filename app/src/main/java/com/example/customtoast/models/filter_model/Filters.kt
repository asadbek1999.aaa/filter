package uz.mobiler.a4kfullwallpapers.models.filter_model

import ja.burhanrashid52.photoeditor.PhotoFilter

data class Filters(var title: String, var photoFilter: PhotoFilter)